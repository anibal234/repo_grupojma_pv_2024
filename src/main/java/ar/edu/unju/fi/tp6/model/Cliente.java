package ar.edu.unju.fi.tp6.model;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component
public class Cliente {

	private String tipoDocumento;
	private int nroDocumento;
	private String Apellido;
	private String Nombre;
	private String email;
	private String password;
	private LocalDate fechaNacimiento;
	private int edad;
	private int codigoAreaTelefono;
	private int nroTeléfono;
	private LocalDate fechaUltimaCompra;
	private int cum;
	private String com;
	private String tim;
	
	
	public Cliente() {
		
	}


	public Cliente(String tipoDocumento, int nroDocumento, String apellido, String nombre, String email,
			String password, LocalDate fechaNacimiento, int edad, int codigoAreaTelefono, int nroTeléfono,
			LocalDate fechaUltimaCompra, int cum, String com, String tim) {
		super();
		this.tipoDocumento = tipoDocumento;
		this.nroDocumento = nroDocumento;
		Apellido = apellido;
		Nombre = nombre;
		this.email = email;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.edad = edad;
		this.codigoAreaTelefono = codigoAreaTelefono;
		this.nroTeléfono = nroTeléfono;
		this.fechaUltimaCompra = fechaUltimaCompra;
		this.cum = cum;
		this.com = com;
		this.tim = tim;
	}


	public String getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public int getNroDocumento() {
		return nroDocumento;
	}


	public void setNroDocumento(int nroDocumento) {
		this.nroDocumento = nroDocumento;
	}


	public String getApellido() {
		return Apellido;
	}


	public void setApellido(String apellido) {
		Apellido = apellido;
	}


	public String getNombre() {
		return Nombre;
	}


	public void setNombre(String nombre) {
		Nombre = nombre;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public int getCodigoAreaTelefono() {
		return codigoAreaTelefono;
	}


	public void setCodigoAreaTelefono(int codigoAreaTelefono) {
		this.codigoAreaTelefono = codigoAreaTelefono;
	}


	public int getNroTeléfono() {
		return nroTeléfono;
	}


	public void setNroTeléfono(int nroTeléfono) {
		this.nroTeléfono = nroTeléfono;
	}


	public LocalDate getFechaUltimaCompra() {
		return fechaUltimaCompra;
	}


	public void setFechaUltimaCompra(LocalDate fechaUltimaCompra) {
		this.fechaUltimaCompra = fechaUltimaCompra;
	}


	public int getCum() {
		return cum;
	}


	public void setCum(int cum) {
		this.cum = cum;
	}


	public String getCom() {
		return com;
	}


	public void setCom(String com) {
		this.com = com;
	}


	public String getTim() {
		return tim;
	}


	public void setTim(String tim) {
		this.tim = tim;
	}
	
	
	
}
