package ar.edu.unju.fi.tp6.service;

import java.time.LocalDate;
import java.util.List;

import ar.edu.unju.fi.tp6.model.Cliente;

public interface IClienteService {
	public void agregarCliente(Cliente nuevoCliente);
	public List<Cliente> listarCliente();
	public int calcularedad(LocalDate fechaNacimiento);
	public String ultimacompra(LocalDate fechaUltimaCompra);
	public String Tiempoedad(LocalDate fechaNacimiento);
	public int fechacumple(LocalDate fechaNacimiento);
	public Cliente obtenerCliente(int nroDocumento);
	public boolean eliminarCliente(int nroDocumento);
}
