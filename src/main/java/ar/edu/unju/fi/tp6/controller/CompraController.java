package ar.edu.unju.fi.tp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.tp6.model.Compra;
import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.ICompraService;
import ar.edu.unju.fi.tp6.service.IProductoService;

@Controller
public class CompraController {
	
	@Autowired
	ICompraService servisecompra;
	
	@Autowired
	IProductoService serviceproducto;
	
		@GetMapping("/addcompra")
		public String getNewEmpleado(Model model) {
			Compra compra = new Compra();
			List<Producto> listpro = serviceproducto.listarProducto(); // s
			model.addAttribute("titulo", "Agregar Compra");
			model.addAttribute("compra", compra);
			model.addAttribute("listpro", listpro); // s
			return "nuedcompra";
		}

		@PostMapping("/guardar")
		public String guardaralumno(Compra compra) {
			servisecompra.agregarcompra(compra);
			return "redirect:/compra";
		}

		@GetMapping("/compra")
		public String getMethodName(Model model) {
			List<Compra> compra = servisecompra.listarcompra();
			model.addAttribute("com", compra);
			return "compras";
		}

		@GetMapping("/editcompra/{id}")
		public String formu4(@PathVariable("id") int id, Model modelo) {
			Compra compra = servisecompra.buscarcompra(id);
			List<Producto> listpro = serviceproducto.listarProducto();
			modelo.addAttribute("titulo", "Editar Compra");
			modelo.addAttribute("compra", compra);  
			modelo.addAttribute("listpro", listpro); 
			return "editcompra";
		}

		@GetMapping("/delecompra/{id}")
		public String formu5(@PathVariable("id") int id, Model modelo) {
			Compra com = servisecompra.buscarcompra(id);
			servisecompra.eliminarcompra(com);
			modelo.addAttribute("com", servisecompra.listarcompra());
			return "redirect:/compra";
		}

}