package ar.edu.unju.fi.tp6.service;

import java.util.List;

import ar.edu.unju.fi.tp6.model.Producto;

public interface IProductoService {

	public void agregarProducto(Producto producto);
	public List<Producto> listarProducto();
	public Producto obtenerProducto(int codigo);
	public boolean eliminarProducto(int codigo);
}
