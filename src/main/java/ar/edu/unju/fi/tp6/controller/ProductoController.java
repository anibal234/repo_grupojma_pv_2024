package ar.edu.unju.fi.tp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.IProductoService;

@Controller
public class ProductoController {

	@Autowired
	IProductoService serviceproducto;

	@GetMapping("/producto")
	public String getNewProducto(Model model){
		Producto producto =new Producto();
		model.addAttribute("nuevoProducto", producto);
		return "nuevo";
	}
	
	@PostMapping("/producto/guardar")
	public String postProducto(Producto nuevoProducto, Model model) {
		serviceproducto.agregarProducto(nuevoProducto);
		List<Producto> producto = serviceproducto.listarProducto();
		model.addAttribute("producto", producto);
		return "resultado";
	}
	
	@GetMapping("/producto/ultimo")
	public String getProducto(Model model) {
		List<Producto> producto = serviceproducto.listarProducto();
		
		for(int contador=0;contador<producto.size();contador++){
			model.addAttribute("ultimoproducto", producto.get(contador));
		}
		return "ultimoproducto";
		}
	@GetMapping("/produc")
	public String produc(Model model){
		List<Producto> producto = serviceproducto.listarProducto();
		model.addAttribute("producto", producto);
		return "resultado";
	}
	
	@GetMapping("/producto/editar/{codigo}")
	public String clienteditar(@PathVariable int codigo, Model model) {
		model.addAttribute("nuevoProducto", serviceproducto.obtenerProducto(codigo));
		return "editarproducto";
	}

	@PostMapping("/productos")
	public String actualizarCliente(@ModelAttribute("nuevoProducto") Producto nuevoProducto) {
		Producto existe = serviceproducto.obtenerProducto(nuevoProducto.getCodigo());
		existe.setCodigo(nuevoProducto.getCodigo());
		existe.setNombre(nuevoProducto.getNombre());
		existe.setPrecio(nuevoProducto.getPrecio());
		existe.setMarca(nuevoProducto.getMarca());
		existe.setStock(nuevoProducto.getStock());
		return "redirect:/produc";
	}

	@GetMapping("/producto/eli/{codigo}")
	public String eliminarCliente(@PathVariable int codigo) {
		serviceproducto.eliminarProducto(codigo);
		return "redirect:/produc";
	}
}
