package ar.edu.unju.fi.tp6.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.IProductoService;

@Service
public class ProductoServiceImp implements IProductoService {
	List<Producto> listaProducto = new ArrayList<>();

	@Override
	public void agregarProducto(Producto producto) {
		listaProducto.add(producto);
	}

	@Override
	public List<Producto> listarProducto() { 
		return listaProducto;
	}
	@Override
	public boolean eliminarProducto(int codigo) {
		boolean encontrado=false;
		for(int contador=0;contador<listaProducto.size();contador++){
		if(listaProducto.get(contador).getCodigo() == (codigo)){
			encontrado=true;
			listaProducto.remove(contador);
			}
		}
		return encontrado;
	}
	@Override
	public Producto obtenerProducto(int codigo) {
		int enc = 0;
		for(int contador=0;contador<listaProducto.size();contador++){
		if(listaProducto.get(contador).getCodigo() == (codigo)){
			enc=contador;
			}
		}
		return listaProducto.get(enc);
	}
}
