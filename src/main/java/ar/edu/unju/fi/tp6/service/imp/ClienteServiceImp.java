package ar.edu.unju.fi.tp6.service.imp;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.tp6.model.Cliente;
import ar.edu.unju.fi.tp6.service.IClienteService;
import ar.edu.unju.fi.tp6.util.listaCliente;

@Service
public class ClienteServiceImp implements IClienteService {
	
	@Autowired
	public listaCliente listaClientes;
	
	@Override
	public void agregarCliente(Cliente nuevoCliente) {
		listaClientes.lista.add(nuevoCliente);
	}
	@Override
	public List<Cliente> listarCliente() {
		return listaClientes.lista;
	}
	
	public LocalTime horactual = LocalTime.now();
	public int horas;
	public int minu;
	public int segu;
	public int encu;
	
	@Override
	public int calcularedad(LocalDate fechaNacimiento) {
		LocalDate fechaactual = LocalDate.now();
		int edad = Period.between(fechaNacimiento, fechaactual).getYears();
		return edad;
	}
	@Override
	public String ultimacompra(LocalDate fechaUltimaCompra) {
		LocalDate fechaActual = LocalDate.now();
		Period periodo = Period.between(fechaUltimaCompra, fechaActual);
        int años = periodo.getYears();
        int meses = periodo.getMonths();
        int dias = periodo.getDays();
        String com =  años + " años, " + meses + " meses y " + dias + " días";
        return com;
	}
	
	@Override
	public String Tiempoedad(LocalDate fechaNacimiento) {
		LocalDate fechaactual2 = LocalDate.now();
		Period periodo = Period.between(fechaNacimiento, fechaactual2);
        int años = periodo.getYears();
        int meses = periodo.getMonths();
        int dias = periodo.getDays();
        horas = horactual.getHour();
        minu = horactual.getMinute();
        segu = horactual.getSecond();
        String com =  dias + " Día " + meses + " Mes " + años + " Año " + horas + " Hora " + minu + " Min " + segu + " Seg" ;
		return com;
	}
	@Override
	public int fechacumple(LocalDate fechaNacimiento) {
		LocalDate fechaactual3 = LocalDate.now();
		long cum = ChronoUnit.DAYS.between(fechaNacimiento, fechaactual3);
		return (int) cum;
	}

	@Override
	public boolean eliminarCliente(int nroDocumento) {
		boolean encontrado=false;
		for(int contador=0;contador<listaClientes.lista.size();contador++){
		if(listaClientes.lista.get(contador).getNroDocumento() == (nroDocumento)){
			encontrado=true;
			listaClientes.lista.remove(contador);
			}
		}
		return encontrado;
	}
	@Override
	public Cliente obtenerCliente(int nroDocumento) {
		int enc = 0;
		for(int contador=0;contador<listaClientes.lista.size();contador++){
		if(listaClientes.lista.get(contador).getNroDocumento() == (nroDocumento)){
			enc=contador;
			}
		}
		return listaClientes.lista.get(enc);
	}
 
}
