package ar.edu.unju.fi.tp6.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp6.model.Compra;
import ar.edu.unju.fi.tp6.service.ICompraService;
import ar.edu.unju.fi.tp6.util.listaCompra;

@Service
public class CompraServiceImp implements ICompraService {

	@Autowired
	public listaCompra listacompra;

	@Override
	public void agregarcompra(Compra compra) {
		Compra com = buscarcompra(compra.getId());
		if (com == null) {
			listacompra.listac.add(compra);
		} else {
			int pos = listacompra.listac.indexOf(com);
			listacompra.listac.set(pos, compra);
		}
	}

	@Override
	public List<Compra> listarcompra() {
		return listacompra.listac;
	}

	@Override
	public Compra buscarcompra(int id) {
		Compra com = null;
		for (Compra coms : listacompra.listac) {
			if (coms.getId() == id) {
				com = coms;
				break;
			}
		}
		return com;
	}

	@Override
	public void eliminarcompra(Compra compra) {
		listacompra.listac.remove(compra);
	}
}
