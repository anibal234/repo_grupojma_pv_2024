package ar.edu.unju.fi.tp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp6CarrilloAnibalAllinApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp6CarrilloAnibalAllinApplication.class, args);
	}

}
