package ar.edu.unju.fi.tp6.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.tp6.model.Cliente;
import ar.edu.unju.fi.tp6.service.IClienteService;

@Controller
public class ClienteController {

	@Autowired
	IClienteService serviceCliente;

	@GetMapping("/index")
	public String inicio() {
		return "index";
	}

	@GetMapping("/cliente/nuevo")
	public String getNewCliente(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("nuevoCliente", cliente);
		return "nuevocliente";
	}

	@PostMapping("/cliente/guardar")
	public String postCliente(Cliente nuevoCliente) {

		int edad = serviceCliente.calcularedad(nuevoCliente.getFechaNacimiento());
		nuevoCliente.setEdad(edad);

		LocalDate fechaUltimaCompra = nuevoCliente.getFechaUltimaCompra();
		String com = serviceCliente.ultimacompra(fechaUltimaCompra);
		nuevoCliente.setCom(com);

		LocalDate fechaNacimiento = nuevoCliente.getFechaNacimiento();
		String tim = serviceCliente.Tiempoedad(fechaNacimiento);
		nuevoCliente.setTim(tim);

		int cum = serviceCliente.fechacumple(nuevoCliente.getFechaNacimiento());
		nuevoCliente.setCum(cum);

		serviceCliente.agregarCliente(nuevoCliente);
		return "redirect:/cliente/listado";
	}

	@GetMapping("/cliente/listado")
	public String gedtCliente(Model model) {
		List<Cliente> cliente = serviceCliente.listarCliente();
		model.addAttribute("cliente", cliente);
		return "clientes";
	}
	

	@GetMapping("/cliente/editar/{nroDocumento}")
	public String clienteditar(@PathVariable("nroDocumento") int nroDocumento, Model model) {
		Cliente cliente = serviceCliente.obtenerCliente(nroDocumento);
		model.addAttribute("nuevoCliente", cliente);
		return "editarcliente";
	}

	@PostMapping("/cliente")
	public String actualizarCliente(@ModelAttribute("nuevoCliente") Cliente nuevoCliente) {
		Cliente existe = serviceCliente.obtenerCliente(nuevoCliente.getNroDocumento());
		existe.setNroDocumento(nuevoCliente.getNroDocumento());
		existe.setApellido(nuevoCliente.getApellido());
		existe.setNombre(nuevoCliente.getNombre());
		existe.setEmail(nuevoCliente.getEmail());
		existe.setPassword(nuevoCliente.getPassword());
		existe.setFechaNacimiento(nuevoCliente.getFechaNacimiento());
		existe.setEdad(nuevoCliente.getEdad());
		existe.setCodigoAreaTelefono(nuevoCliente.getCodigoAreaTelefono());
		existe.setNroTeléfono(nuevoCliente.getNroTeléfono());
		existe.setFechaUltimaCompra(nuevoCliente.getFechaUltimaCompra());
		int edad = serviceCliente.calcularedad(nuevoCliente.getFechaNacimiento());
		existe.setEdad(edad);

		LocalDate fechaUltimaCompra = nuevoCliente.getFechaUltimaCompra();
		String com = serviceCliente.ultimacompra(fechaUltimaCompra);
		existe.setCom(com);

		LocalDate fechaNacimiento = nuevoCliente.getFechaNacimiento();
		String tim = serviceCliente.Tiempoedad(fechaNacimiento);
		existe.setTim(tim);

		int cum = serviceCliente.fechacumple(nuevoCliente.getFechaNacimiento());
		existe.setCum(cum);
		return "redirect:/cliente/listado";
	}

	@GetMapping("/cliente/eli/{nroDocumento}")
	public String eliminarCliente(@PathVariable int nroDocumento) {
		serviceCliente.eliminarCliente(nroDocumento);
		return "redirect:/cliente/listado";
	}
}
