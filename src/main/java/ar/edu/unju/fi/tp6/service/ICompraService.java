package ar.edu.unju.fi.tp6.service;

import java.util.List;

import ar.edu.unju.fi.tp6.model.Compra;

public interface ICompraService {

	public void agregarcompra(Compra compra);
	public List<Compra> listarcompra();
	public Compra buscarcompra(int id);
	public void eliminarcompra(Compra compra);
}
